<?php

// block.tags written by dman (user/33240)
// Allows inline blocks through a BBcode-like syntax
// Things will display nicely if you add some CSS to support them such as:
/*
  <style>
  .node .content .block {
    width:200px;
    float:right;
    margin:1em;
    padding:1em;
    border:1px solid gray;
  }
  </style>
*/

function _reptag_block_init($context) {
  return array(
  
    "#{BLOCK:([0-9]+)}#e"          => "theme('block', (object)module_invoke('block', 'block', 'view', '\\1'));",
    "#{BLOCK:(.*?):([0-9]+)}#e"    => "theme('block', (object)module_invoke('\\1', 'block', 'view', '\\2'));",
    "#{BLOCK:(.*?):(.+?)}#e"       => "theme('block', (object)_reptag_block_by_title('\\2','\\1'));",
    "#{BLOCK:(.+?)}#e"             => "theme('block', (object)_reptag_block_by_title('\\1'));"
    
  );
}

function _reptag_block_info() {
  return array(t('Inline Blocks ({BLOCK:1}, {BLOCK:menu:2} ...)'), FALSE);
}

function _reptag_block_help() {
  return array(
  
    "{BLOCK:1}"                   => "user-defined block 1",
    "{BLOCK:Contact Details}"     => "user-defined block with the given title",
    "{BLOCK:menu:2}"              => "block 2 supplied by the named module",
    "{BLOCK:image:Random image}"  => "named block from the given module"
    
  );
}

function _reptag_block_by_title($subject, $module = 'block') {
  $list = module_invoke($module, 'block', 'list');
  foreach ($list as $key => $block) {
    // seach by either admin block label or blocks display label
    if ($block['info'] == $subject) {
      return module_invoke($module, 'block', 'view', $key);
    }
    $block = module_invoke($module, 'block', 'view', $key);
    if ($key == $subject) {
      return $block;
    }
    if ($block['subject'] == $subject) {
      return $block;
    }
  }
}

/**
 * Call this to add the basic CSS to the page
 */
function _reptag_block_add_css() {
  static $done;
  if (!$done) {
    drupal_set_html_head("
      <style>
      .node .content .block{
        width:200px;
        float:right;
        margin:1em;
        padding:1em;
        border:1px solid gray;
      }
      </style>
    ");
  }
  
  $done = TRUE;
}
