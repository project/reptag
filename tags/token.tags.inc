<?php

function _reptag_token_init($context) {
  return array('');
}

function _reptag_token_info() {
  return array(t('Token (Integration of the Token module)'), FALSE);
}

function _reptag_token_help() {
  return theme_token_help('node');
}

function _reptag_token_require() {
  return module_exists('token');  
}

function _reptag_token_process($text, $tags, $context) {  
  return token_replace($text, 'node', $context);
}
